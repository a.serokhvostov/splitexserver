package com.splitexserver.backend;

public class ConnectionSettings {
    public static final String JDBC_DRIVER = org.h2.Driver.class.getName();
    public static final String JDBC_URL = "jdbc:h2:mem:proddb";
    public static final String USER = "sa";
    public static final String PASSWORD = "password";
}
