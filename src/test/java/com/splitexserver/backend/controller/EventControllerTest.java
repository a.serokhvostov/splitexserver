package com.splitexserver.backend.controller;

import java.util.List;

import com.splitexserver.backend.BackendApplicationTest;
import com.splitexserver.backend.dto.EventDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

public class EventControllerTest extends BackendApplicationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void get() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events?email=o.olegov@gmail.com",
                String.class)).contains("event 1");
    }

    @Test
    public void create() {
        this.restTemplate.postForEntity("http://localhost:" + port + "/events/create?email=o.olegov@gmail.com",
                new EventDto("new event", "bla bla", List.of("a.ann@gmail.com"), "2021-12-20"),String.class);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events?email=o.olegov@gmail.com",
                String.class)).contains("bla bla").contains("ORGANIZER");
    }

    @Test
    public void changeToPrepared() {
        this.restTemplate.put("http://localhost:" + port + "/events/0/prepared", null);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events?email=o.olegov@gmail.com",
                String.class)).contains("PREPARED");
    }

    @Test
    public void getGoods() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events/0/goods",
                String.class)).contains("Cola");
    }
}
