package com.splitexserver.backend.controller;

import com.splitexserver.backend.BackendApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

public class DebtControllerTest extends BackendApplicationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void findDebtsFromEmail() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/debts/income?email=b.bob@gmail.com",
                String.class)).contains("a.ann@gmail.com");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/debts/income?email=a.ann@gmail.com",
                String.class)).doesNotContain("b.bob@gmail.com");
    }

    @Test
    public void findDebtsToEmail() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/debts/outcome?email=b.bob@gmail.com",
                String.class)).doesNotContain("a.ann@gmail.com");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/debts/outcome?email=a.ann@gmail.com",
                String.class)).contains("b.bob@gmail.com");
    }

    @Test
    public void changeDebtStatusToCompleted() {
        this.restTemplate.put("http://localhost:" + port + "/debts/0/close",
                null);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/debts/outcome?email=a.ann@gmail.com",
                String.class)).doesNotContain("222");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events?email=i.ivanov@gmail.com",
                String.class)).contains("COMPLETED");
    }
}
