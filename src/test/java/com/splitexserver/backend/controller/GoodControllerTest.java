package com.splitexserver.backend.controller;

import com.splitexserver.backend.BackendApplicationTest;
import com.splitexserver.backend.dto.GoodDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

public class GoodControllerTest extends BackendApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void getGoodInformationTest(){
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/goods/0",
                String.class)).contains("Cola");
    }

    @Test
    void addGoodToEventTest() {
        this.restTemplate.postForEntity("http://localhost:" + port + "/events/0?email=a.ann@gmail.com",
                new GoodDto("testGood", 12424, "this is test good") ,String.class);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/events/0/goods",
                String.class)).contains("testGood");
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/goods/51/payers",
                String.class)).contains("a.ann@gmail.com");
    }

    @Test
    void addUserInPayerForGoodTest() {
        this.restTemplate.postForEntity("http://localhost:" + port + "/goods/0/payers?email=a.ann@gmail.com",
                null ,String.class);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/goods/0/payers",
                String.class)).contains("a.ann@gmail.com");
    }

    @Test
    void getGoodsPayersTest() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/goods/0/payers",
                String.class)).contains("a.smirnov@gmail.com");
    }

    @Test
    void changeGoodTest() {
        this.restTemplate.put("http://localhost:" + port + "/goods/0",
                new GoodDto("Cola", 232, "test"));
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/goods/0",
                String.class)).contains("232");
    }
}
