package com.splitexserver.backend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "friends")
public class Friend {
    @EmbeddedId
    private Key key = new Key();

    @ManyToOne
    @MapsId("userEmailFirst")
    @JoinColumn(name = "user_email_1")
//    @Column(name = "user_email_1")
    private User userFirst;

    @ManyToOne
    @MapsId("userEmailSecond")
    @JoinColumn(name = "user_email_2")
//    @Column(name = "user_email_2")
    private User userSecond;

    @Column(name = "is_confirmed")
    private boolean isConfirmed;

    public Friend(Key key, User userFirst, User userSecond, boolean isConfirmed) {
        this.key = key;
        this.userFirst = userFirst;
        this.userSecond = userSecond;
        this.isConfirmed = isConfirmed;
    }

    public Friend() {

    }

    public User getUserFirst() {
        return userFirst;
    }

    public void setUserFirst(User userFirst) {
        this.userFirst = userFirst;
    }

    public User getUserSecond() {
        return userSecond;
    }

    public void setUserSecond(User userSecond) {
        this.userSecond = userSecond;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    @Embeddable
    public static class Key implements Serializable {
        @Column(name = "user_email_1")
        private String userEmailFirst;

        @Column(name = "user_email_2")
        private String userEmailSecond;

        public String getUserEmailFirst() {
            return userEmailFirst;
        }

        public void setUserEmailFirst(String userIdFirst) {
            this.userEmailFirst = userIdFirst;
        }

        public String getUserEmailSecond() {
            return userEmailSecond;
        }

        public void setUserEmailSecond(String userIdSecond) {
            this.userEmailSecond = userIdSecond;
        }

    }

}
