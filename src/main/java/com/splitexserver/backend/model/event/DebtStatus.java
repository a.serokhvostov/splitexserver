package com.splitexserver.backend.model.event;


public enum DebtStatus {
    WAITING_FOR_PAYING, PAID
}