package com.splitexserver.backend.model.event;

public enum Role {
    PARTICIPANT, ORGANIZER
}
