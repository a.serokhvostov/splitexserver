package com.splitexserver.backend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users_goods")
public class UserGood {

    @EmbeddedId
    @JsonIgnore
    private UserGood.Key key = new UserGood.Key();

    @ManyToOne
    @MapsId("goodIdKey")
    @JoinColumn(name = "good_id")
    @JsonIgnore
    private Good goodIdKey;

    @ManyToOne
    @MapsId("userEmailKey")
    @JoinColumn(name = "user_email")
    private User userEmailKey;

    @Embeddable
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Key implements Serializable {
        @Column(name = "good_id")
        private Long goodIdKey;

        @Column(name = "user_email")
        private String userEmailKey;

        public Long getGoodIdKey() {
            return goodIdKey;
        }

        public void setGoodIdKey(Long goodIdKey) {
            this.goodIdKey = goodIdKey;
        }

        public String getUserEmailKey() {
            return userEmailKey;
        }

        public void setUserEmailKey(String userEmailKey) {
            this.userEmailKey = userEmailKey;
        }
    }
}
