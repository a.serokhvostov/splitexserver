package com.splitexserver.backend.model;

import java.util.List;

import com.splitexserver.backend.model.event.UsersEvents;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserWithDebt extends User{
    private double debt;

    public UserWithDebt(String email, String firstName, String lastName, String phoneNumber, String bankDetails,
                        String avatar, List<UsersEvents> users, double debt) {
        super(email, firstName, lastName, phoneNumber, bankDetails, avatar, users);
        this.debt = debt;
    }
}
