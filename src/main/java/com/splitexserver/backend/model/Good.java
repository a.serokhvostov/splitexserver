package com.splitexserver.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "goods")
public class Good {

    @Id
    @SequenceGenerator(name = "good_sequence", sequenceName = "event_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "good_sequence")
    @Column(name = "good_id", unique = true)
    private Long goodId;

    @Column(name = "event_id")
    private Long eventId;

    @Column(name = "creator_user_email")
    private String creatorUserEmail;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "description")
    private String description;

}
