package com.splitexserver.backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.splitexserver.backend.model.event.UsersEvents;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
//    @Id
//    @Column(name = "user_id", length = 6, nullable = false)
//    private long id;

    @Id
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "bank_details")
    private String bankDetails;

    @Column(name = "avatar")
    private String avatar;

    // @OneToMany(mappedBy="userFirst", cascade=CascadeType.ALL, orphanRemoval = true)
    // @JsonIgnore
    // private Set<Friend> friends = new HashSet<>();
//    @OneToMany(mappedBy="userFirst", cascade=CascadeType.ALL, orphanRemoval = true)
//    private Set<Friend> friends = new HashSet<>();

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<UsersEvents> users;
}