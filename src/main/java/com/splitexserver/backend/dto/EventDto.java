package com.splitexserver.backend.dto;

import java.util.List;

import lombok.Data;

@Data
public class EventDto {
    private final String name;
    private final String description;
    private final List<String> userEmails;
    private final String date;
}
