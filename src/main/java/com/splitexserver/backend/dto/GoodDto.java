package com.splitexserver.backend.dto;

import lombok.Data;

@Data
public class GoodDto {
    private final String name;
    private final double price;
    private final String description;
}
