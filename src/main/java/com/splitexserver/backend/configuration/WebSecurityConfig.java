package com.splitexserver.backend.configuration;

import com.splitexserver.backend.security.GoogleOAuth2UserService;
import com.splitexserver.backend.service.UserService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final GoogleOAuth2UserService oAuth2UserService;
    private final UserService userService;

    public WebSecurityConfig(GoogleOAuth2UserService oAuth2UserService, UserService userService) {
        this.oAuth2UserService = oAuth2UserService;
        this.userService = userService;
    }

    @Override
    public void configure(final WebSecurity web) {
        web.ignoring().antMatchers("/**");
        web.ignoring().antMatchers("/v3/api-docs/**",
                "/swagger-ui/**",
                "/swagger-ui.html");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//
//        http.authorizeRequests()
//                .antMatchers( "/","/login", "/oauth/**"/*, "/friends", "/events"*/).permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin().permitAll();
//
//        http.oauth2Login()
//                .loginPage("/")
//                .userInfoEndpoint()
//                .userService(oAuth2UserService)
//                .and()
//                .successHandler((request, response, authentication) -> {
//                    GoogleOAuth2User oAuth2User = new GoogleOAuth2User((OAuth2User) authentication.getPrincipal());
//                    userService.processOAuthPostLogin(oAuth2User.getName(),oAuth2User.getName(),oAuth2User.getEmail
//                    (), "88005553535");
//                    response.sendRedirect("/");
//                });
//
    }


}
