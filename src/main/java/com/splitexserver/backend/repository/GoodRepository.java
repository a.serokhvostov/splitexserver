package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.Good;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface GoodRepository extends JpaRepository<Good, Long> {
    List<Good> findAllByEventId(long eventId);

    @Transactional
    @Modifying
    @Query(value = "update Good g set g.description = :description, g.name = :name, g.price = " +
            ":price where g.goodId = :good_id")
    void updateGood(@Param("good_id") Long goodId, @Param("name") String name, @Param("price") double price,
                    @Param("description") String description);
}
