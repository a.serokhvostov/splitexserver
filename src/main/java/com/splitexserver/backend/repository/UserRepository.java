package com.splitexserver.backend.repository;

import java.util.Optional;

import com.splitexserver.backend.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("select U from User U where U.phoneNumber = :phone_number")
    User findByNumber(@Param("phone_number") String number);

    @Query("SELECT U FROM User U WHERE U.email = :email")
    Optional<User> getUserByEmail(@Param("email") String email);

    @Query(value = "SELECT U FROM User U WHERE U.firstName LIKE CONCAT('%',:firstName,'%') AND U.lastName LIKE CONCAT" +
            "('%',:lastName,'%')",
            countQuery = "SELECT count(U) FROM User U WHERE U.firstName LIKE CONCAT('%',:firstName,'%') AND U" +
                    ".lastName LIKE CONCAT('%',:lastName,'%')")
    Page<User> searchUserByName(@Param("firstName") String firstName, @Param("lastName") String lastName,
                                Pageable pageable);
}