package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.Debt;
import com.splitexserver.backend.model.User;
import com.splitexserver.backend.model.event.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface DebtRepository extends JpaRepository<Debt, Long> {
    @Query(value =
            "select D from Debt D where D.status = 'WAITING_FOR_PAYING' AND D.debtorEmail = :email")
    List<Debt> findIncomeDebts(@Param("email") User email);

    @Query(value =
            "select D from Debt D where D.status = 'WAITING_FOR_PAYING' AND D.userToPayEmail = :email")
    List<Debt> findOutcomeDebts(@Param("email") User email);

    List<Debt> findAllByEventId(Event eventId);
}
