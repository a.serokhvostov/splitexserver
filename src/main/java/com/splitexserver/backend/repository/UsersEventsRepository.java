package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.event.UsersEvents;
import com.splitexserver.backend.model.event.UsersEventsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersEventsRepository extends JpaRepository<UsersEvents, UsersEventsId> {
    List<UsersEvents> findAllByEventEventId(Long eventId);
}
