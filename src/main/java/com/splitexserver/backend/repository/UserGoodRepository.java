package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.UserGood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserGoodRepository extends JpaRepository<UserGood, UserGood.Key> {

    public List<UserGood> findAllByGoodIdKey_GoodId(long goodId);
}
