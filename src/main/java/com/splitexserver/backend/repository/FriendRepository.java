package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.Friend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface FriendRepository extends JpaRepository<Friend, Friend.Key> {
    @Query(value =
            "select F from Friend F where (F.key.userEmailFirst = :user_email or F.key.userEmailSecond = :user_email)" +
                    " and F.isConfirmed = true")
    List<Friend> findFriendsByEmail(@Param("user_email") String user_email);

    @Query(value =
            "select F from Friend F where (F.key.userEmailFirst = :emailUser and F.key.userEmailSecond = " +
                    ":emailFriend) or" +
                    " (F.key.userEmailFirst = :emailFriend and F.key.userEmailSecond = :emailUser)")
    Friend findFriendByEmail(@Param("emailUser") String emailUser, @Param("emailFriend") String emailFriend);

    @Query(value = "select F from Friend F where F.key.userEmailSecond = :user_email and F.isConfirmed = false",
            countQuery = "select count(F) from Friend F where F.key.userEmailSecond = :user_email and F.isConfirmed =" +
                    " false")
    Page<Friend> findPotentialFriendsByEmail(@Param("user_email") String user_email, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value =
            "delete from Friend F where (F.key.userEmailFirst = :emailUser and F.key.userEmailSecond = :emailFriend) " +
                    "or" +
                    " (F.key.userEmailFirst = :emailFriend and F.key.userEmailSecond = :emailUser)")
    void deleteFriend(@Param("emailUser") String emailUser, @Param("emailFriend") String emailFriend);

    @Transactional
    @Modifying
    @Query(value =
            "update Friend F set F.isConfirmed = :isConfirmed where F.key.userEmailFirst = :emailUser and " +
                    "F.key.userEmailSecond = :emailFriend")
    void updateIsConfirmedFriend(@Param("emailUser") String emailUser, @Param("emailFriend") String emailFriend,
                                 @Param("isConfirmed") Boolean isConfirmed);

    @Query(value =
            "select F from Friend F where F.key.userEmailFirst = :email and F.userSecond.phoneNumber = :phone")
    Friend findFriendByPhone(@Param("email") String email, @Param("phone") String phone);

//    @Transactional
//    @Modifying
//    @Query(value =
//            "delete from Friend F where F.key.userEmailFirst = :email and F.userSecond.phoneNumber = :phone")
//    void deleteFriendByPhone(@Param("email") String email, @Param("phone") String phone);

}
