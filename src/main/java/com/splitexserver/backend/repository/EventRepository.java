package com.splitexserver.backend.repository;

import java.util.List;

import com.splitexserver.backend.model.event.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    @Query(value = "select E from UsersEvents UE join Event E on E.eventId = UE.event.eventId " +
            "where UE.user.email = :email")
    List<Event> getEventsByUserEmail(@Param("email") String email);
}
