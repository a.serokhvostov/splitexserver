package com.splitexserver.backend.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.splitexserver.backend.dto.EventDto;
import com.splitexserver.backend.exception.ResourceNotFoundException;
import com.splitexserver.backend.model.Debt;
import com.splitexserver.backend.model.Good;
import com.splitexserver.backend.model.User;
import com.splitexserver.backend.model.UserGood;
import com.splitexserver.backend.model.event.DebtStatus;
import com.splitexserver.backend.model.event.Event;
import com.splitexserver.backend.model.event.EventStatus;
import com.splitexserver.backend.model.event.Role;
import com.splitexserver.backend.model.event.UsersEvents;
import com.splitexserver.backend.repository.DebtRepository;
import com.splitexserver.backend.repository.EventRepository;
import com.splitexserver.backend.repository.GoodRepository;
import com.splitexserver.backend.repository.UsersEventsRepository;
import org.springframework.stereotype.Service;

@Service
public class EventService {
    private final EventRepository eventRepository;
    private final UserService userService;
    private final UsersEventsRepository usersEventsRepository;
    private final GoodRepository goodRepository;
    private final UserGoodService userGoodService;
    private final DebtRepository debtRepository;

    public EventService(EventRepository eventRepository, UserService userService,
                        UsersEventsRepository usersEventsRepository, GoodRepository goodRepository,
                        UserGoodService userGoodService, DebtRepository debtRepository) {
        this.eventRepository = eventRepository;
        this.userService = userService;
        this.usersEventsRepository = usersEventsRepository;
        this.goodRepository = goodRepository;
        this.userGoodService = userGoodService;
        this.debtRepository = debtRepository;
    }

    public long createEvent(EventDto eventDto, String creatorEmail) {
        Event event = new Event();
        event.setName(eventDto.getName());
        event.setDate(LocalDate.parse(eventDto.getDate()));
        event.setDescription(eventDto.getDescription());
        event.setEventStatus(EventStatus.PREPARING);

        eventRepository.save(event);
        for (String userEmail : eventDto.getUserEmails()) {
            User user = userService.getUserByEmail(userEmail);
            UsersEvents usersEvents = new UsersEvents(event, user, Role.PARTICIPANT);
            usersEventsRepository.save(usersEvents);

        }
        User user = userService.getUserByEmail(creatorEmail);
        UsersEvents usersEvents = new UsersEvents(event, user, Role.ORGANIZER);
        usersEventsRepository.save(usersEvents);

        return event.getEventId();
    }

    public Event getById(long id) {
        return eventRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("event", id));
    }

    public List<Good> getGoods(long eventId) {
        return goodRepository.findAllByEventId(eventId);
    }

    public void changeStatus(long eventId, EventStatus newStatus) {
        Event event = getById(eventId);
        event.setEventStatus(newStatus);
        eventRepository.save(event);
        if (newStatus.equals(EventStatus.PREPARED)) {
            calculateDebts(eventId);
        }
    }

    public List<Event> getEventsByUserEmail(String email) {
        return eventRepository.getEventsByUserEmail(email);
    }

    private void calculateDebts(long eventId) {
        List<Good> goods = goodRepository.findAllByEventId(eventId);
        Map<String, Map<String, Double>> debts = new HashMap<>();
        for (Good good : goods) {
            List<UserGood> userGoods = userGoodService.findByGoodId(good.getGoodId());
            double price = good.getPrice() / userGoods.size();
            String creditorEmail = good.getCreatorUserEmail();


            for (UserGood userGood : userGoods) {
                String debtorEmail = userGood.getUserEmailKey().getEmail();
                if (!debtorEmail.equals(creditorEmail)) {
                    Map<String, Double> creditsOutcome = debts.get(creditorEmail);
                    if (creditsOutcome != null && creditsOutcome.containsKey(debtorEmail)) {
                        creditsOutcome.compute(debtorEmail, (k, v) -> v == null ? price : v + price);
                    } else {
                        Map<String, Double> creditsIncome = debts.get(debtorEmail);
                        if (creditsIncome != null && creditsIncome.containsKey(creditorEmail)) {
                            double currentDebt = creditsIncome.get(creditorEmail);
                            if (currentDebt == price) {
                                creditsIncome.remove(creditorEmail);
                            } else if (currentDebt < price) {
                                creditsIncome.remove(creditorEmail);
                                double newDebt = price - currentDebt;
                                if (creditsOutcome == null) {
                                    creditsOutcome = new HashMap<>();
                                    debts.put(creditorEmail, creditsOutcome);
                                }
                                creditsOutcome.compute(debtorEmail, (k, v) -> v == null ? newDebt : v + newDebt);
                            } else {
                                creditsIncome.put(creditorEmail, currentDebt - price);
                            }
                        } else {
                            if (creditsOutcome == null) {
                                creditsOutcome = new HashMap<>();
                                debts.put(creditorEmail, creditsOutcome);
                            }
                            creditsOutcome.compute(debtorEmail, (k, v) -> v == null ? price : v + price);
                        }
                    }
                }
            }
        }
        Event event = getById(eventId);
        debts.forEach((creditorEmail, creditorDebts) ->
                creditorDebts.forEach((debtorEmail, debtSum) ->
                        debtRepository.save(new Debt(null, debtSum, userService.getUserByEmail(debtorEmail),
                                userService.getUserByEmail(creditorEmail), event,
                                DebtStatus.WAITING_FOR_PAYING))));
    }
}
