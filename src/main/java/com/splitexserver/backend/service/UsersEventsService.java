package com.splitexserver.backend.service;

import java.util.List;
import java.util.Optional;

import com.splitexserver.backend.model.event.UsersEvents;
import com.splitexserver.backend.model.event.UsersEventsId;
import com.splitexserver.backend.repository.UsersEventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersEventsService {
    @Autowired
    private UsersEventsRepository usersEventsRepository;

    public List<UsersEvents> findAllByEventId(Long eventId) {
        return usersEventsRepository.findAllByEventEventId(eventId);
    }

    public Optional<UsersEvents> findByEventIdAndUserEmail(Long eventId, String userEmail) {
        var key = new UsersEventsId();
        key.setEvent(eventId);
        key.setUser(userEmail);
        return usersEventsRepository.findById(key);
    }

    public UsersEvents save(UsersEvents usersEvents) {
        return usersEventsRepository.save(usersEvents);
    }
}
