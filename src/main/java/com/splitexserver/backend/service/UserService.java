package com.splitexserver.backend.service;

import java.util.Optional;

import com.splitexserver.backend.exception.ResourceNotFoundException;
import com.splitexserver.backend.model.User;
import com.splitexserver.backend.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    public User getById(long id) {
//        return userRepository.getById(id);
//    }

    public User getByNumber(String number) {
        return userRepository.findByNumber(number);
    }

    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email).orElseThrow(() -> new ResourceNotFoundException("user", email));
    }

    public boolean existById(String email) {
        return userRepository.existsById(email);
    }

    public Page<User> searchUserByName(String firstName, String lastName, Pageable pageable) {
        return userRepository.searchUserByName(firstName, lastName, pageable);
    }

    public void signUpUser(User user) {
        userRepository.save(user);
    }

    public User getUserByPhone(String userPhone) {
        return userRepository.findByNumber(userPhone);
    }
}
