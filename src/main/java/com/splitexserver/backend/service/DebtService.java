package com.splitexserver.backend.service;

import java.util.List;

import com.splitexserver.backend.exception.ResourceNotFoundException;
import com.splitexserver.backend.model.Debt;
import com.splitexserver.backend.model.event.DebtStatus;
import com.splitexserver.backend.model.event.EventStatus;
import com.splitexserver.backend.repository.DebtRepository;
import org.springframework.stereotype.Service;

@Service
public class DebtService {
    private final DebtRepository debtRepository;
    private final UserService userService;
    private final EventService eventService;

    public DebtService(DebtRepository debtRepository, UserService userService, EventService eventService) {
        this.debtRepository = debtRepository;
        this.userService = userService;
        this.eventService = eventService;
    }

    public Debt save(Debt debt) {
        return debtRepository.save(debt);
    }

    public void closeDebt(long debtId) {
        Debt debt = getById(debtId);
        debt.setStatus(DebtStatus.PAID);
        debtRepository.save(debt);
        List<Debt> debtsInEvent = debtRepository.findAllByEventId(debt.getEventId());
        boolean allPaid = debtsInEvent.stream().allMatch(d -> d.getStatus().equals(DebtStatus.PAID));
        if (allPaid) {
            eventService.changeStatus(debt.getEventId().getEventId(), EventStatus.COMPLETED);
        }
    }

    public List<Debt> findIncomeDebts(String email) {
        var user = userService.getUserByEmail(email);
        return debtRepository.findIncomeDebts(user);
    }

    public List<Debt> findOutcomeDebts(String email) {
        var user = userService.getUserByEmail(email);
        return debtRepository.findOutcomeDebts(user);
    }

    public Debt getById(long id) {
        return debtRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("debt", id));
    }


}
