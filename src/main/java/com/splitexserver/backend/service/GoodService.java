package com.splitexserver.backend.service;

import java.util.List;
import java.util.Optional;

import com.splitexserver.backend.dto.GoodDto;
import com.splitexserver.backend.exception.ResourceNotFoundException;
import com.splitexserver.backend.model.Good;
import com.splitexserver.backend.model.UserGood;
import com.splitexserver.backend.model.event.Event;
import com.splitexserver.backend.model.event.EventStatus;
import com.splitexserver.backend.repository.EventRepository;
import com.splitexserver.backend.repository.GoodRepository;
import org.springframework.stereotype.Service;

@Service
public class GoodService {
    private final GoodRepository goodRepository;
    private final UserGoodService userGoodService;
    private final UserService userService;

    public GoodService(GoodRepository goodRepository, UserGoodService userGoodService, UserService userService) {
        this.goodRepository = goodRepository;
        this.userGoodService = userGoodService;
        this.userService = userService;
    }

    public List<Good> findGoodsByEventId(Long eventId) {
        return goodRepository.findAllByEventId(eventId);
    }

    public Good getGoodInformation(long goodId) {
        return goodRepository.findById(goodId).orElseThrow(() -> new ResourceNotFoundException("good", goodId));
    }


    public long addGoodToEvent(Long eventId, GoodDto goodDto, String creatorEmail) {

        Good newGood = new Good(null, eventId, creatorEmail, goodDto.getName(),
                goodDto.getPrice(), goodDto.getDescription());

        newGood = goodRepository.save(newGood);
        userGoodService.addGoodToUser(new UserGood(new UserGood.Key(newGood.getGoodId(), creatorEmail),
                newGood, userService.getUserByEmail(creatorEmail)));
        return newGood.getGoodId();
    }

    public void updateGood(Long goodId, GoodDto goodDto) {
        goodRepository.updateGood(goodId, goodDto.getName(), goodDto.getPrice(), goodDto.getDescription());
    }
}
