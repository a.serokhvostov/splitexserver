package com.splitexserver.backend.service;

import java.util.List;
import java.util.Optional;

import com.splitexserver.backend.model.Friend;
import com.splitexserver.backend.repository.FriendRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FriendService {
    private final FriendRepository friendRepository;

    public FriendService(FriendRepository friendRepository) {
        this.friendRepository = friendRepository;
    }

    public List<Friend> getFriendsByEmail(String email) {
        List<Friend> friends = friendRepository.findFriendsByEmail(email);
        return friends;
    }

    public Page<Friend> findPotentialFriendsByEmail(String email, Pageable pageable) {
        return friendRepository.findPotentialFriendsByEmail(email, pageable);
    }

    public Friend getFriendByEmail(String emailUser, String emailFriend) {
        return friendRepository.findFriendByEmail(emailUser, emailFriend);
    }

    public Friend getFriendByPhone(String emailUser, String phone) {
        return friendRepository.findFriendByPhone(emailUser, phone);
    }

    public void deleteFriend(String emailUser, String emailFriend) {
        if (friendRepository.findFriendByEmail(emailUser, emailFriend).isConfirmed()) {
            friendRepository.deleteFriend(emailUser, emailFriend);
            //change to false in second user
            friendRepository.updateIsConfirmedFriend(emailFriend, emailUser, false);
        } else {
            friendRepository.deleteFriend(emailUser, emailFriend);
        }
    }

    public void deleteFriendByPhone(String email, String phone) {
        Friend friend = friendRepository.findFriendByPhone(email, phone);
        if (friend.isConfirmed()) {
            friendRepository.deleteFriend(email, friend.getUserSecond().getEmail());
            //change to false in second user
            friendRepository.updateIsConfirmedFriend(friend.getUserSecond().getEmail(), email, false);
        } else {
            friendRepository.deleteFriend(email, friend.getUserSecond().getEmail());
        }
    }

    public Friend save(Friend recipeModel) {
        return friendRepository.save(recipeModel);
    }

    public boolean existById(Friend.Key key) {
        return friendRepository.existsById(key);
    }

    public Optional<Friend> findFriendRequestByKey(Friend.Key key) {
        return friendRepository.findById(key);
    }

}
