package com.splitexserver.backend.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.splitexserver.backend.model.User;
import com.splitexserver.backend.model.UserGood;
import com.splitexserver.backend.model.UserWithDebt;
import com.splitexserver.backend.repository.GoodRepository;
import com.splitexserver.backend.repository.UserGoodRepository;
import org.springframework.stereotype.Service;

@Service
public class UserGoodService {
    private final GoodRepository goodRepository;
    private final UserGoodRepository userGoodRepository;

    public UserGoodService(UserGoodRepository userGoodRepository, GoodRepository goodRepository) {
        this.userGoodRepository = userGoodRepository;
        this.goodRepository = goodRepository;
    }

    public Optional<UserGood> findByUserEmailAndGoodId(String userEmail, Long goodId) {
        var key = new UserGood.Key();
        key.setGoodIdKey(goodId);
        key.setUserEmailKey(userEmail);
        return userGoodRepository.findById(key);
    }

    public List<UserGood> findByGoodId(long goodId) {
        return userGoodRepository.findAllByGoodIdKey_GoodId(goodId);
    }

    public List<UserWithDebt> getPayers(long goodId) {
        double price = goodRepository.getById(goodId).getPrice();
        List<User> users = findByGoodId(goodId)
                .stream()
                .map(UserGood::getUserEmailKey)
                .collect(Collectors.toList());
        return users.stream().map(u -> new UserWithDebt(u.getEmail(), u.getFirstName(), u.getLastName(), u.getPhoneNumber(),
                u.getBankDetails(), u.getAvatar(), u.getUsers(), price/users.size())).collect(Collectors.toList());
    }

    public UserGood addGoodToUser(UserGood userGoodModel) {
        return userGoodRepository.save(userGoodModel);
    }
}
