package com.splitexserver.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String entity, long id) {
        super(entity + " : " + id);
    }

    public ResourceNotFoundException(String entity, String id) {
        super(entity + " : " + id);
    }
}