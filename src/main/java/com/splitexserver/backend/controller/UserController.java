package com.splitexserver.backend.controller;

import com.splitexserver.backend.model.User;
import com.splitexserver.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/search/firstName={firstName}&lastName={lastName}")
    public Page<User> searchUserByName(@PathVariable String firstName,
                                       @PathVariable String lastName,
                                       Pageable pageable) {
        return userService.searchUserByName(firstName, lastName, pageable);
    }

    @GetMapping("/user/get")
    public User getUserByLogin(@RequestParam(value = "email") String email) {
        return userService.getUserByEmail(email);
    }

    @PostMapping("/user/signup")
    public HttpStatus signUp(@RequestBody User user) {
        userService.signUpUser(user);
        return HttpStatus.OK;
    }

    @GetMapping("/user/signin")
    public RedirectView signIn() {
        return new RedirectView("splitexapp://oauth.redirect");
    }
}
