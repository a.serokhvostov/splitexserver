package com.splitexserver.backend.controller;

import java.util.List;

import com.splitexserver.backend.dto.GoodDto;
import com.splitexserver.backend.model.Good;
import com.splitexserver.backend.model.UserGood;
import com.splitexserver.backend.model.UserWithDebt;
import com.splitexserver.backend.service.GoodService;
import com.splitexserver.backend.service.UserGoodService;
import com.splitexserver.backend.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodController {
    private final GoodService goodService;

    private final UserService userService;

    private final UserGoodService userGoodService;

    public GoodController(GoodService goodService, UserService userService, UserGoodService userGoodService) {
        this.goodService = goodService;
        this.userService = userService;
        this.userGoodService = userGoodService;
    }

    @GetMapping("/goods/{good_id}")
    public Good getGoodInformation(@PathVariable(name = "good_id") long goodId) {
        return goodService.getGoodInformation(goodId);
    }

    @PostMapping("/events/{event_id}")
    public long addGoodToEvent(@PathVariable(name = "event_id") long eventId, @RequestBody GoodDto goodDto,
                               @RequestParam(value = "email") String email) {

        return goodService.addGoodToEvent(eventId, goodDto, email);
    }

    @PostMapping("/goods/{good_id}/payers")
    public UserGood addUserInPayerForGood(@PathVariable(value = "good_id") long goodId,
                                          @RequestParam(value = "email") String email) {
        Good good = goodService.getGoodInformation(goodId);
        UserGood userGood = new UserGood(new UserGood.Key(goodId, email), good, userService.getUserByEmail(email));
        userGood = userGoodService.addGoodToUser(userGood);

        return userGood;
    }

    @GetMapping("/goods/{good_id}/payers")
    public List<UserWithDebt> getGoodsPayers(@PathVariable(value = "good_id") long goodId) {
        return userGoodService.getPayers(goodId);
    }

    @PutMapping("/goods/{good_id}")
    public void changeGood(@PathVariable(value = "good_id") Long goodId,
                           @RequestBody GoodDto goodDto) {
        goodService.updateGood(goodId, goodDto);
    }
}
