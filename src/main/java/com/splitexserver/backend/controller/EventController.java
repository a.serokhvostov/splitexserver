package com.splitexserver.backend.controller;

import java.util.List;

import com.splitexserver.backend.dto.EventDto;
import com.splitexserver.backend.model.Good;
import com.splitexserver.backend.model.event.Event;
import com.splitexserver.backend.model.event.EventStatus;
import com.splitexserver.backend.service.EventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventController {
    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/events/create")
    public long createEvent(@RequestBody EventDto eventDto,
                            @RequestParam(name = "email") String creatorEmail) {
        return eventService.createEvent(eventDto, creatorEmail);
    }

    @GetMapping("/events/{id}/goods")
    public List<Good> getGoods(@PathVariable(name = "id") long eventId) {
        return eventService.getGoods(eventId);
    }

    @PutMapping("/events/{event_id}/prepared")
    public void changeToPrepared(@PathVariable(name = "event_id") long eventId) {
        eventService.changeStatus(eventId, EventStatus.PREPARED);
    }

    @GetMapping("/events")
    public List<Event> getEventsByUser(@RequestParam(value = "email") String email) {
        return eventService.getEventsByUserEmail(email);
    }


//    private String getEmailByAuthToken(){
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (principal instanceof UserDetails) {
//            String user = ((UserDetails)principal).getUsername();
//            return user.substring(user.indexOf(", email=") + 8, user.indexOf("}]"));
//        } else {
//            String user = principal.toString();
//            return user.substring(user.indexOf(", email=") + 8, user.indexOf("}]"));
//        }
//    }
}
