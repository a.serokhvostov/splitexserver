package com.splitexserver.backend.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.splitexserver.backend.model.Friend;
import com.splitexserver.backend.model.User;
import com.splitexserver.backend.service.FriendService;
import com.splitexserver.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FriendController {
    @Autowired
    private FriendService friendService;
    @Autowired
    private UserService userService;

    @GetMapping("/user/friends")
    public List<User> getFriends(@RequestParam(value = "email") String email) {
        List<Friend> friends = friendService.getFriendsByEmail(email);
        return friends.stream().flatMap(f -> Stream.of(f.getUserFirst(), f.getUserSecond()))
                .filter(u -> !u.getEmail().equals(email))
                .collect(Collectors.toList());
    }

    @PostMapping("/user/friends/{friend_email}/remove")
    public void deleteFriend(@RequestParam(value = "email") String email,
                             @RequestParam(value = "friend_email") String friendEmail) {
        friendService.deleteFriend(email, friendEmail);
    }

    @GetMapping("/invites")
    public Page<Friend> getPotentionalFriends(@RequestParam(value = "email") String email, Pageable pageable) {
        return friendService.findPotentialFriendsByEmail(email, pageable);
    }

    @PostMapping("/invites/send")
    public Friend sendInvite(@RequestParam(value = "email") String email,
                             @RequestParam(value = "friendEmail") String friendEmail) {

        Friend friend = new Friend();
        Friend.Key key = new Friend.Key();
        key.setUserEmailFirst(email);
        key.setUserEmailSecond(friendEmail);
        friend.setKey(key);
        friend.setUserFirst(userService.getUserByEmail(email));
        friend.setUserSecond(userService.getUserByEmail(friendEmail));
        friend.setConfirmed(false);
        return friendService.save(friend);

    }

    @PutMapping("/invites/answer")
    public Friend answerInvite(@RequestParam(value = "email") String email,
                               @RequestParam(value = "friendEmail") String friendEmail,
                               @RequestParam(value = "answer") boolean answer) {
        Friend.Key key = new Friend.Key();
        key.setUserEmailFirst(friendEmail);
        key.setUserEmailSecond(email);

        var friend = friendService.findFriendRequestByKey(key).get();
        friend.setConfirmed(answer);
        return friendService.save(friend);


    }

    @GetMapping("/user/friends/info")
    public ResponseEntity<User> getFriendInfo(@RequestHeader(value = "Email") String email,
                                              @RequestParam(value = "userPhone") String userPhone) {
        if (userService.getUserByEmail(email) != null) {
            if (friendService.getFriendByPhone(email, userPhone) != null) {
                User friend = userService.getUserByPhone(userPhone);
                return new ResponseEntity<>(friend, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
