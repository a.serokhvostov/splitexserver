package com.splitexserver.backend.controller;

import java.util.List;

import com.splitexserver.backend.model.Debt;
import com.splitexserver.backend.service.DebtService;
import com.splitexserver.backend.service.EventService;
import com.splitexserver.backend.service.GoodService;
import com.splitexserver.backend.service.UserGoodService;
import com.splitexserver.backend.service.UserService;
import com.splitexserver.backend.service.UsersEventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DebtController {
    private final DebtService debtService;

    public DebtController(DebtService debtService) {
        this.debtService = debtService;
    }


    @GetMapping("/debts/income")
    public List<Debt> findDebtsFromEmail(@RequestParam("email") String email) {
        return debtService.findIncomeDebts(email);
    }

    @GetMapping("/debts/outcome")
    public List<Debt> findDebtsToEmail(@RequestParam("email") String email) {
        return debtService.findOutcomeDebts(email);
    }

    @PutMapping("/debts/{id}/close")
    public void changeDebtStatusToCompleted(@PathVariable("id") Long debtId) {
        debtService.closeDebt(debtId);
    }

}
