CREATE TABLE IF NOT EXISTS users
(
--     user_id      bigint NOT NULL,
    email        varchar(50) NOT NULL,
    first_name   varchar(50) NOT NULL,
    last_name    varchar(50) NOT NULL,
    phone_number varchar(30),
    bank_details varchar(30),
    avatar       varchar(300),
--     CONSTRAINT pk_user_id_users_table PRIMARY KEY ( user_id )
    CONSTRAINT pk_user_id_users_table PRIMARY KEY (email)
);
--
-- CREATE TABLE IF NOT EXISTS roles
-- (
--     role_id      bigint NOT NULL,
--     name         varchar(30),
--     CONSTRAINT pk_role_id_roles_table PRIMARY KEY ( role_id )
--     );
--
-- CREATE TABLE IF NOT EXISTS users_roles
-- (
--     user_id      bigint NOT NULL,
--     role_id      bigint NOT NULL,
--     CONSTRAINT pk_users_roles_table PRIMARY KEY ( user_id, role_id ),
--     CONSTRAINT fk_user_id_users_roles_table FOREIGN KEY ( user_id ) REFERENCES users ( user_id ),
--     CONSTRAINT fk_role_id_users_roles_table FOREIGN KEY ( role_id ) REFERENCES roles ( role_id )
--     );
--
CREATE TABLE IF NOT EXISTS events
(
    event_id    bigint       NOT NULL,
    name        varchar(100) NOT NULL,
    date        date         NOT NULL,
--     unique_identifier varchar(50) NOT NULL,
    description text         NOT NULL,
    status      varchar(30)  NOT NULL,
    CONSTRAINT pk_event_id_events_table PRIMARY KEY (event_id)
);

CREATE TABLE IF NOT EXISTS goods
(
    good_id            bigint           NOT NULL,
    name               varchar(50)      NOT NULL,
    price              double precision NOT NULL,
    description        varchar(50),

    creator_user_email varchar(50)      NOT NULL,
    event_id           bigint           NOT NULL,
    CONSTRAINT pk_good_id_goods_table PRIMARY KEY (good_id),
    CONSTRAINT fk_creator_user_id_goods_table FOREIGN KEY (creator_user_email) REFERENCES users (email),
    CONSTRAINT fk_event_id_goods_table FOREIGN KEY (event_id) REFERENCES events (event_id)
);

CREATE TABLE IF NOT EXISTS debts
(
    debt_id           serial           NOT NULL,
    debt_sum          double precision NOT NULL,
    status            varchar(30)      NOT NULL,

    debtor_email      varchar(50)      NOT NULL,
    user_to_pay_email varchar(50)      NOT NULL,
    event_id          bigint           NOT NULL,
    CONSTRAINT pk_debt_id_debts_table PRIMARY KEY (debt_id),
    CONSTRAINT fk_debtor_id_debts_table FOREIGN KEY (debtor_email) REFERENCES users (email),
    CONSTRAINT fk_user_to_pay_id_debts_table FOREIGN KEY (user_to_pay_email) REFERENCES users (email),
    CONSTRAINT fk_event_id_debts_table FOREIGN KEY (event_id) REFERENCES events (event_id)
);

CREATE TABLE IF NOT EXISTS friends
(
    user_email_1 varchar(50) NOT NULL,
    user_email_2 varchar(50) NOT NULL,
    is_confirmed boolean     NOT NULL,
    CONSTRAINT pk_friends_table PRIMARY KEY (user_email_1, user_email_2),
    CONSTRAINT fk_user_1_id_friends_table FOREIGN KEY (user_email_1) REFERENCES users (email),
    CONSTRAINT fk_user_2_id_friends_table FOREIGN KEY (user_email_2) REFERENCES users (email)
);


CREATE TABLE IF NOT EXISTS users_events
(
    event_id bigint      NOT NULL,
    email    varchar(50) NOT NULL,
    role     varchar(30) NOT NULL,
    CONSTRAINT pk_users_events_table PRIMARY KEY (email, event_id),
    CONSTRAINT fk_user_email_users_events_table FOREIGN KEY (email) REFERENCES users (email),
    CONSTRAINT fk_event_id_users_events_table FOREIGN KEY (event_id) REFERENCES events (event_id)
);

CREATE TABLE IF NOT EXISTS users_goods
(
    good_id    bigint      NOT NULL,
    user_email varchar(50) NOT NULL,
    CONSTRAINT pk_users_goods_table PRIMARY KEY (user_email, good_id),
    CONSTRAINT fk_user_id_users_goods_table FOREIGN KEY (user_email) REFERENCES users (email),
    CONSTRAINT fk_good_id_users_goods_table FOREIGN KEY (good_id) REFERENCES goods (good_id)
);

