insert into users values ('i.ivanov@gmail.com', 'Ivan', 'Ivanov', '88888888888', '', ''),
                         ('o.olegov@gmail.com', 'Oleg', 'Olegov', '77777777777', '', ''),
                         ('a.smirnov@gmail.com', 'Anatoliy', 'Smirnov', '66666666666', '', ''),
                         ('b.bob@gmail.com', 'Bob', 'Bob', '55555555555', '', ''),
                         ('a.ann@gmail.com', 'Ann', 'Ann', '44444444444', '', '');
insert into friends values ('i.ivanov@gmail.com', 'o.olegov@gmail.com', true),
                           ('o.olegov@gmail.com', 'i.ivanov@gmail.com', true),
                           ('i.ivanov@gmail.com', 'a.smirnov@gmail.com', true),
                           ('a.smirnov@gmail.com', 'i.ivanov@gmail.com',true),
                           ('b.bob@gmail.com', 'a.ann@gmail.com', true),
                           ('a.ann@gmail.com', 'b.bob@gmail.com',true);
insert into events values (0, 'event 1', '2021-11-11', 'description 1', 'PREPARING'),
                          (1, 'event 2', '2021-12-12', 'description 2', 'PREPARING'),
                          (2, 'event 3', '2021-12-12', 'description 3', 'PREPARED');
insert into users_events values (0, 'o.olegov@gmail.com', 'PARTICIPANT'),
                                (0, 'a.smirnov@gmail.com', 'ORGANIZER'),
                                (1, 'b.bob@gmail.com', 'PARTICIPANT'),
                                (1, 'a.ann@gmail.com', 'ORGANIZER'),
                                (2, 'i.ivanov@gmail.com', 'ORGANIZER');
insert into goods values (0, 'Cola', 500, '', 'i.ivanov@gmail.com', 0);
insert into users_goods values (0, 'o.olegov@gmail.com'),
                               (0, 'i.ivanov@gmail.com'),
                               (0, 'a.smirnov@gmail.com');
insert into debts values (0, 222, 'WAITING_FOR_PAYING', 'b.bob@gmail.com', 'a.ann@gmail.com', 2),
                         (1, 500, 'WAITING_FOR_PAYING', 'b.bob@gmail.com', 'a.ann@gmail.com', 0);

